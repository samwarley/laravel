#!/bin/bash

# This script pushes laravel application images to ECR, then builds on Fargate
# AWS environment variables must be set before running
# export AWS_ACCESS_KEY_ID=
# export AWS_SECRET_ACCESS_KEY=
# export AWS_DEFAULT_REGION=

# Ensure in default context to allow docker push
docker context use default

# Create ECR repositories if they don't yet exist
aws ecr describe-repositories --repository-names php || aws ecr create-repository --repository-name php
aws ecr describe-repositories --repository-names nginx || aws ecr create-repository --repository-name nginx

# Push images to ECR repositories
docker push 827330632269.dkr.ecr.eu-west-1.amazonaws.com/nginx
docker push 827330632269.dkr.ecr.eu-west-1.amazonaws.com/php

# Log in to ECR
aws ecr get-login-password | docker login --username AWS --password-stdin 827330632269.dkr.ecr.eu-west-1.amazonaws.com

# Create and switch to ECS context
echo "AWS environment variables" | docker context create ecs remote
docker context use remote

# Build laravel application
docker compose up nginx mysql php

# Run artisan migrate
docker compose up artisan

docker compose ps

echo 'Build complete, access using the nginx service address.'
