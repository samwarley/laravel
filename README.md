# docker-compose-laravel
A pretty simplified Docker Compose workflow that sets up a LEMP network of containers for local Laravel development.
Modified from https://github.com/aschmelyun/docker-compose-laravel based on the full article that inspired this repo [here](https://dev.to/aschmelyun/the-beauty-of-docker-for-local-laravel-development-13c0).


## Usage

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this, and spin up the containers for the web server by running:

- `docker-compose build`
- `docker-compose up -d nginx mysql php`

The following are built for our web server, with their exposed ports detailed:

- **nginx** - `:80`
- **mysql** - `:3306`
- **php** - `:9000`

In order to run `artisan migrate` and the tests use the additional containers:

- `docker-compose up artisan`
- `docker-compose up tests`
