#!/bin/bash

# This script builds the laravel application, runs the tests and shuts down

docker-compose build
docker-compose up -d nginx mysql php

# Replace with test to check mysql has come up
sleep 10

# Run artisan migrate
docker-compose up artisan

# Run tests
docker-compose up tests

docker-compose down
